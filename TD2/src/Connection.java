
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Connection
 */
@WebServlet("/Connection")
public class Connection extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Connection() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		Cookie[] cookies = request.getCookies();
		for (int i = 0; cookies != null && i < cookies.length; i++) {
			if ("lastAccessTime".equals(cookies[i].getName())) {
				long l = Long.parseLong(cookies[i].getValue());
				try (PrintWriter out = response.getWriter()) {
					out.write("</br>");
					out.write("Last time access：" + new Date(l).toString());
				}

			}
		}
		Cookie ck = new Cookie("lastAccessTime", System.currentTimeMillis() + "");
		response.addCookie(ck);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Set<Integer> keys = UserManager.getUsersTable().keySet();
		int found = -1;
		for (Integer key : keys) {
			System.out
					.println(UserManager.getUsersTable().get(key).getLogin().equals(request.getParameter("username")));
			System.out.println(UserManager.getUsersTable().get(key).getPwd().equals(request.getParameter("password")));
			System.out.println(key);
			if (UserManager.getUsersTable().get(key).getLogin().equals(request.getParameter("username"))
					&& UserManager.getUsersTable().get(key).getPwd().equals(request.getParameter("password"))) {
				found = key;
				break;
			}
		}
		if (found == -1) {
			response.setContentType("text/html;charset=UTF-8");
			try (PrintWriter out = response.getWriter()) {
				out.println("<!DOCTYPE html>");
				out.println("<html>");
				out.println("<head>");
				out.println("<title>Servlet Connection</title>");
				out.println("</head>");
				out.println("<body>");
				out.println("<h1>Failed, wrong username or password  </h1>");
				out.println("</body>");
				out.println("</html>");
			}
		} else {
			HttpSession session = request.getSession();
			session.setAttribute("login", UserManager.getUsersTable().get(found).getLogin());
			session.setAttribute("role", UserManager.getUsersTable().get(found).getRole());

			response.setContentType("text/html;charset=UTF-8");
			if (session.getAttribute("role").equals("admin")) {
				try (PrintWriter out = response.getWriter()) {
					out.println("<!DOCTYPE html>");
					out.println("<html><head><title>Navigation</title></head>");
					out.println("<body>");
					out.println("<h1>Hello " + session.getAttribute("login") + "</h1>");
					out.println("<nav> <ul>");
					out.println("<li>Connected</li>");
					out.println("<li><a href=\"newUser.html\"> Create a new User</a></li>");
					out.println("<li><a href=\"UserManager\"> Show all users</a></li>");
					out.println("</ul>");
					out.println("</nav>");
					out.println("</body>");
					out.println("</html>");
					doGet(request, response);
				}
			}
			try (PrintWriter out = response.getWriter()) {
				out.println("<!DOCTYPE html>");
				out.println("<html>");
				out.println("<head>");
				out.println("<title>Servlet Connecion</title>");
				out.println("</head>");
				out.println("<body>");
				out.println("<h1>Succes </h1>");
				out.println("</body>");
				out.println("</html>");
			}
		}
	}

}
